
const option1 = document.querySelector('.option1'),
      option2 = document.querySelector('.option2'),
      option3 = document.querySelector('.option3'),
      option4 = document.querySelector('.option4');
      

const optionElements = document.querySelectorAll('.option');

const question = document.getElementById('question');

const numberOfQuestion = document.getElementById('number-of-question'),
      numberOfAllQuestions = document.getElementById('number-of-all-questions');

let indexOfQuestion,
    indexOfPage = 0;

const answersTracker = document.getElementById('answers-tracker');
const btnNext = document.getElementById('btn-next');

let score = 0;

const correctAnswer = document.getElementById('correct-answer'),
      numberOfAllQuestions2 = document.getElementById('number-of-all-questions-2'),
      btnTryAgain = document.getElementById('btn-try-again');

const questions = [
    {
        question: 'Где можно использовать JavaScript?',
        options: [
            'серверные приложения',
            'веб-приложения',
            'мобильные приложения',
            'во всех перечисленных',
        ],
        rightAnswer: 3
    },
    {
        question: 'В чем отличие между локальной и глобальной переменной?',
        options: [
            'отличий нет',
            'локальные видны повсюду, глобальные только в функциях',
            'глобальные видны повсюду, локальные только в функциях',
            'глобальные можно переопределять, локальные нельзя',
        ],
        rightAnswer: 2
    },
    {
        question: 'Какая переменная записана неверно?',
        options: [
            'var num = "STRING";',
            'var isDone = 0;',
            'var b = false;',
            'var number = 12,5;',
        ],
        rightAnswer: 3
    },
    {
        question: 'Язык JavaScript является подвидом языка Java - верно?',
        options: [
            'да',
            'нет',
            'наоборот, Java - подвид JavaScript',
            'Java и JavaScript - это одно и тоже',
        ],
        rightAnswer: 1
    },
    {
        question: 'В чем разница между confirm и prompt?',
        options: [
            'ничем не отличаются',
            'confirm вызывает диалоговое окно с полем для ввода, prompt - окно с подтверждением',
            'prompt вызывает диалоговое окно с полем для ввода, confirm - окно с подтверждением',
            'prompt и confirm в JavaScript не существует',
        ],
        rightAnswer: 2
    },
    {
        question: 'JSON - это...',
        options: [
            'JavaScript Object Notation',
            'название следующей версии JavaScript',
            'JavaScript Over Network',
            'имя создателя JavaScript',
        ],
        rightAnswer: 0
    },
    {
        question: 'Расшифруйте аббревиатуру DOM.',
        options: [
            'Document Object Modified',
            'Digital Optical Modulation',
            'Document Object Model',
            'Domestic Object Mode',
        ],
        rightAnswer: 2
    },
];

numberOfAllQuestions.innerHTML = questions.length;

const load = () => {
    question.innerHTML = questions[indexOfQuestion].question;


    option1.innerHTML = questions[indexOfQuestion].options[0];
    option2.innerHTML = questions[indexOfQuestion].options[1];
    option3.innerHTML = questions[indexOfQuestion].options[2];
    option4.innerHTML = questions[indexOfQuestion].options[3];
    
    numberOfQuestion.innerHTML = indexOfPage + 1;
    indexOfPage++;
};

let compLetedAnswers = [];

const randomQuestion = () => {
    let randomNumber = Math.floor(Math.random() * questions.length);
    let hitDuplicate = false;

    if(indexOfPage == questions.length) {
        quizOver()
    } else {
        if(compLetedAnswers.length > 0) {
            compLetedAnswers.forEach(item => {
                if(item == randomNumber) {
                    hitDuplicate = true;
                }
            });
            if(hitDuplicate) {
                randomQuestion();
            } else {
                indexOfQuestion = randomNumber;
                load();
            }
        }
        if(compLetedAnswers.length == 0) {
            indexOfQuestion = randomNumber;
            load();
        }
    }
    compLetedAnswers.push(indexOfQuestion);
};

const chekAnswer = el => {
    if(el.target.dataset.id == questions[indexOfQuestion].rightAnswer) {
        el.target.classList.add('correct');
        updateAnswerTracker('correct');
        score++;
    } else {
        el.target.classList.add('wrong');
        updateAnswerTracker('wrong');
    }
    disabledoptions();
}

for(option of optionElements) {
    option.addEventListener('click' , e => chekAnswer(e));
}


const disabledoptions = () => {
    optionElements.forEach(item => {
        item.classList.add('disabled');
        if(item.dataset.id == questions[indexOfQuestion].rightAnswer) {
            item.classList.add('correct');
        }
    })
}


const enableOptions = () => {
    optionElements.forEach(item => {
        item.classList.remove('disabled', 'correct', 'wrong');
    })
};

const answerTracker = () => {
    questions.forEach(() => {
        const div = document.createElement('div');
        answersTracker.appendChild(div);
    })
};

const updateAnswerTracker = status => {
    answersTracker.children[indexOfPage - 1].classList.add(`${status}`);
}

const validate = () => {
    if(!optionElements[0].classList.contains('disabled')){
        alert('Вам нужно выбрать один из вариантов ответов');
    } else {
        randomQuestion();
        enableOptions();
    }
}

const quizOver = () => {
    document.querySelector('.quiz-over-modal').classList.add('active');
    document.querySelector('.content').classList.add('active');
    correctAnswer.innerHTML = score;
    numberOfAllQuestions2.innerHTML = questions.length;
};

const tryAgain = () => {
    window.location.reload();
};

btnTryAgain.addEventListener('click', tryAgain);

btnNext.addEventListener('click', () => {
    validate();
})

window.addEventListener('load', () => {
    randomQuestion();
    answerTracker();
})